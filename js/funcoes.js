// 
$('.panel-clr').bind('click', function() {
	$(this).toggleClass('on');
});

$('.depoimento-1').bind('click', function() {
	$('.depoimento-1').addClass('active');
	$('.depoimento-2, .depoimento-3').removeClass('active');
});
$('.depoimento-2').bind('click', function() {
	$('.depoimento-2').addClass('active');
	$('.depoimento-1, .depoimento-3').removeClass('active');
});
$('.depoimento-3').bind('click', function() {
	$('.depoimento-3').addClass('active');
	$('.depoimento-2, .depoimento-1').removeClass('active');
});

// 
$(document).ready(function(){
	$('.carousel-diferenciais').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 3000,
		arrows: false,
		dots: true,
		pauseOnHover: false,
		responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 2
			}
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 1
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1
			}
		}
		]
	});
});

// Escolha seu diferencial
$(document).ready(function(){
	$('.carousel-seu-ambiente').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		fade: true,
		autoplay: true,
		autoplaySpeed: 3000,
		arrows: false,
		dots: true,
		pauseOnHover: false,
	});
});